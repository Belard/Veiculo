﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Veiculo
{
    public partial class Form1 : Form
    {
        Veiculo veiculo;
        public Form1()
        {
            InitializeComponent();
        }

        private void ButtonCriarVeiculo_Click(object sender, EventArgs e)
        {
            veiculo = new Veiculo("HH-23-89", 23678, 40.5, 4);
            LabelQtsKmPode.Text = veiculo.QuantosKms().ToString();
        }

        private void ButtonRegistarViagem_Click(object sender, EventArgs e)
        {
            double KmViagem;
            if (!double.TryParse(TextBoxKmViagem.Text.Trim(), out KmViagem))
            {
                MessageBox.Show("Valor inválido!");
                TextBoxKmViagem.Text = "";
                TextBoxKmViagem.Focus();
            }
            else
            {
                veiculo.Quilometragem += KmViagem;
                LabelKmTotal.Text = veiculo.Quilometragem.ToString();
                TextBoxKmViagem.Text = "";
                // viagens realizadas
                veiculo.ViagensNum = veiculo.ViagensNum + 1;
                LabelViagensRealizadas.Text = veiculo.ViagensNum.ToString();
            }
                
        }
    }
}
