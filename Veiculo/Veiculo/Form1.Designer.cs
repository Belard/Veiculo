﻿namespace Veiculo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonCriarVeiculo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LabelQtsKmPode = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelKmTotal = new System.Windows.Forms.Label();
            this.ButtonRegistarViagem = new System.Windows.Forms.Button();
            this.TextBoxKmViagem = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelLitrosDeposito = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LabelViagensRealizadas = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ButtonCriarVeiculo
            // 
            this.ButtonCriarVeiculo.Location = new System.Drawing.Point(444, 239);
            this.ButtonCriarVeiculo.Name = "ButtonCriarVeiculo";
            this.ButtonCriarVeiculo.Size = new System.Drawing.Size(75, 39);
            this.ButtonCriarVeiculo.TabIndex = 0;
            this.ButtonCriarVeiculo.Text = "Criar Veículo";
            this.ButtonCriarVeiculo.UseVisualStyleBackColor = true;
            this.ButtonCriarVeiculo.Click += new System.EventHandler(this.ButtonCriarVeiculo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Quantos Kms pode percorrer:";
            // 
            // LabelQtsKmPode
            // 
            this.LabelQtsKmPode.AutoSize = true;
            this.LabelQtsKmPode.Location = new System.Drawing.Point(187, 31);
            this.LabelQtsKmPode.Name = "LabelQtsKmPode";
            this.LabelQtsKmPode.Size = new System.Drawing.Size(22, 13);
            this.LabelQtsKmPode.TabIndex = 2;
            this.LabelQtsKmPode.Text = ". . .";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Quilometragem total:";
            // 
            // LabelKmTotal
            // 
            this.LabelKmTotal.AutoSize = true;
            this.LabelKmTotal.Location = new System.Drawing.Point(145, 59);
            this.LabelKmTotal.Name = "LabelKmTotal";
            this.LabelKmTotal.Size = new System.Drawing.Size(22, 13);
            this.LabelKmTotal.TabIndex = 4;
            this.LabelKmTotal.Text = ". . .";
            // 
            // ButtonRegistarViagem
            // 
            this.ButtonRegistarViagem.Location = new System.Drawing.Point(190, 239);
            this.ButtonRegistarViagem.Name = "ButtonRegistarViagem";
            this.ButtonRegistarViagem.Size = new System.Drawing.Size(80, 39);
            this.ButtonRegistarViagem.TabIndex = 5;
            this.ButtonRegistarViagem.Text = "Registar viagem";
            this.ButtonRegistarViagem.UseVisualStyleBackColor = true;
            this.ButtonRegistarViagem.Click += new System.EventHandler(this.ButtonRegistarViagem_Click);
            // 
            // TextBoxKmViagem
            // 
            this.TextBoxKmViagem.Location = new System.Drawing.Point(39, 249);
            this.TextBoxKmViagem.Name = "TextBoxKmViagem";
            this.TextBoxKmViagem.Size = new System.Drawing.Size(100, 20);
            this.TextBoxKmViagem.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(145, 252);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Km";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nº litros no depósito:";
            // 
            // LabelLitrosDeposito
            // 
            this.LabelLitrosDeposito.AutoSize = true;
            this.LabelLitrosDeposito.Location = new System.Drawing.Point(146, 87);
            this.LabelLitrosDeposito.Name = "LabelLitrosDeposito";
            this.LabelLitrosDeposito.Size = new System.Drawing.Size(22, 13);
            this.LabelLitrosDeposito.TabIndex = 9;
            this.LabelLitrosDeposito.Text = ". . .";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Viagens realizadas:";
            // 
            // LabelViagensRealizadas
            // 
            this.LabelViagensRealizadas.AutoSize = true;
            this.LabelViagensRealizadas.Location = new System.Drawing.Point(140, 115);
            this.LabelViagensRealizadas.Name = "LabelViagensRealizadas";
            this.LabelViagensRealizadas.Size = new System.Drawing.Size(22, 13);
            this.LabelViagensRealizadas.TabIndex = 11;
            this.LabelViagensRealizadas.Text = ". . .";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 309);
            this.Controls.Add(this.LabelViagensRealizadas);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LabelLitrosDeposito);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TextBoxKmViagem);
            this.Controls.Add(this.ButtonRegistarViagem);
            this.Controls.Add(this.LabelKmTotal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LabelQtsKmPode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonCriarVeiculo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonCriarVeiculo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabelQtsKmPode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LabelKmTotal;
        private System.Windows.Forms.Button ButtonRegistarViagem;
        private System.Windows.Forms.TextBox TextBoxKmViagem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LabelLitrosDeposito;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LabelViagensRealizadas;
    }
}

