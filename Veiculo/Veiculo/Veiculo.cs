﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Veiculo
{
    class Veiculo
    {
        #region Atributos

        private double _consumoMedio = 4.5;
        private double _reserva = 10;
        #endregion

        #region Propriedades

        public string Matricula { get; set; }
        public double Quilometragem { get; set; }
        public double Capacidade { get; set; }
        public double CombustivelContido { get; set; }
        public double ConsumoMedio
        {
            get { return _consumoMedio; }
        }
        public double Reserva
        {
            get { return _reserva; }
        }
        public int ViagensNum { get; set; }

        #endregion

        #region Métodos

        public Veiculo(string matricula, double quilometragem, double capacidade, double combustivelContido)
        {
            Matricula = matricula;
            Quilometragem = quilometragem;
            Capacidade = capacidade;
            CombustivelContido = combustivelContido;
        }

        public double QuantosKms()
        {
            double qtsKms;

            qtsKms = 100 * CombustivelContido / ConsumoMedio;

            return qtsKms;
        }

        #endregion
    }
}
